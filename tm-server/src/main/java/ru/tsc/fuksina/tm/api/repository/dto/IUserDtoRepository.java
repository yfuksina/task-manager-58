package ru.tsc.fuksina.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.dto.model.UserDto;

public interface IUserDtoRepository extends IRepositoryDto<UserDto> {

    @Nullable
    UserDto findOneByLogin(@NotNull String login);

    @Nullable
    UserDto findOneByEmail(@NotNull String email);

    void removeByLogin(@NotNull String login);

}
