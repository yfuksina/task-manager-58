package ru.tsc.fuksina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.fuksina.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.fuksina.tm.comparator.CreatedComparator;
import ru.tsc.fuksina.tm.comparator.DateStartComparator;
import ru.tsc.fuksina.tm.comparator.StatusComparator;
import ru.tsc.fuksina.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void add(@NotNull final String userId, final @NotNull M model) {
        if (userId.isEmpty()) return;
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.remove(model);
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.merge(model);
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateStartComparator.INSTANCE) return "status";
        else return "name";
    }

}
