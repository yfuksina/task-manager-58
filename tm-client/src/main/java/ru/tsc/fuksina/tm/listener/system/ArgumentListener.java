package ru.tsc.fuksina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.fuksina.tm.event.ConsoleEvent;
import ru.tsc.fuksina.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class ArgumentListener extends AbstractSystemListListener {

    @NotNull
    public static final String NAME = "argument";

    @NotNull
    public static final String DESCRIPTION = "Show application arguments";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@argumentListener.getName() == #event.name || @argumentListener.getArgument() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @NotNull final Collection<AbstractListener> commands = getArguments();
        for (final AbstractListener command : commands) {
            @NotNull final String name = command.getName();
            if (name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
